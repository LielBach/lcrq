// Copyright (c) 2013, Adam Morrison and Yehuda Afek.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  * Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
//  * Neither the name of the Tel Aviv University nor the names of the
//    author of this software may be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Ported to C11 by Dror Dayan and Liel Bach 2017

#include <threads.h>
#include <stdio.h>
#include "../include/stdatomic.h"
#include "../include/model-assert.h"
#include <stdlib.h>
 


#ifndef RING_POW
#define RING_POW        (5)
#endif
#define RING_SIZE       (1ull << RING_POW)
const uint64_t idxMask=(~(1ull << 63));
const uint64_t bit63Mask=((1ull << 63));

#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)

//README: the Node is not atomic, atomicity is given by the pointer to the Node;
//make sure only CAS on pointers are in use and that you don't modify a node inner fields

typedef struct RingNode {
    uint64_t val;
    uint64_t idx;
    uint64_t pad[14];
} RingNode __attribute__ ((aligned (128)));

typedef struct RingQueue {
    atomic_uint_fast64_t head __attribute__ ((aligned (128)));
    atomic_uint_fast64_t tail __attribute__ ((aligned (128)));
    atomic_uintptr_t next __attribute__ ((aligned (128)));//equvivalent to volatile RingQueue*
    atomic_uintptr_t array[RING_SIZE];//equvivalent to volatile const RingNode* array[RING_SIZE];
} RingQueue __attribute__ ((aligned (128)));



atomic_uintptr_t head;//equvivalent to volatile RingQueue*
atomic_uintptr_t tail;//equvivalent to volatile RingQueue*

_Thread_local uint64_t mycloses;
_Thread_local uint64_t myunsafes;



//---------------helper functions
inline void *getMemory(size_t size){
    void *pointer = malloc(size);
    if(pointer == NULL){
        perror("malloc failed");
        exit(1);
    } 
    
    return pointer;
}

inline void fixState(RingQueue *rq) {
    while (1) {
        uint64_t t = atomic_load(&rq->tail);
        uint64_t h = atomic_load(&rq->head);

        if (unlikely(atomic_load(&rq->tail) != t))
            continue;

        if (h > t) {
            if (atomic_compare_exchange_strong(&rq->tail, &t, h)) break;
            continue;
        }
        break;
    }
}

inline RingNode* createNode(const uint64_t newVal,const uint64_t newIdx){
	RingNode* newNode=getMemory(sizeof(RingNode));
    newNode->val=newVal;
    newNode->idx = newIdx;
	return newNode;
}

//CASNode is a solution for atomic CAS of 128bits when the architecture only allows 64bits CAS
//the solution is to CAS a pointer to a Node instead of the full node
inline int CASNode(atomic_uintptr_t* nodePtr,const RingNode* oldNode,const uint64_t newVal,const uint64_t newIdx){
    RingNode* newNode=createNode(newVal,newIdx);
    return atomic_compare_exchange_strong(nodePtr,(uint64_t*)&oldNode,newNode);
}

//The original implementation of BIT_TEST_AND_SET uses inline assembly and is wrong (sets bit 63 regardless of the b parameter)
//this is my implementation
inline int BIT_63_TEST_AND_SET(atomic_uint_fast64_t* ptr){
    uint64_t old=atomic_load(ptr);//updates by atomic_compare_exchange_strong no need to reload in each while iteration
    while( !atomic_compare_exchange_strong(ptr,&old, old|bit63Mask) ){}
    return !(old&bit63Mask);
}


inline int crq_is_closed(const uint64_t t) {
    return (t & bit63Mask) != 0;
}
inline int is_empty(uint64_t v)  {
    return (v == (uint64_t)-1);
}
inline uint64_t node_index(uint64_t i) {
    return (i & idxMask);
}
inline uint64_t node_unsafe(uint64_t i) {
    return (i & bit63Mask);
}
inline int close_crq(RingQueue *rq, const uint64_t t, const int tries) {
    if (tries < 10){
        uint64_t temp=t+1;
        return atomic_compare_exchange_strong(&(rq->tail), &temp, (t + 1)|bit63Mask);
    } else {
        return BIT_63_TEST_AND_SET(&rq->tail);
    }
}
inline void count_closed_crq(void) {
    mycloses++;
}
inline void count_unsafe_node(void) {
    myunsafes++;
}
inline uint64_t set_unsafe(const uint64_t i){
    return (i & idxMask);
}
inline uint64_t tail_index(const uint64_t t){
    return t & idxMask;
}


//--------------------init functions-------------------------------
inline void init_ring_element(atomic_uintptr_t* node, uint64_t index){
    RingNode* newNode=createNode(-1,index);
    atomic_store(node,newNode);
}

inline void init_ring(RingQueue *r) {
    for (uint64_t index = 0; index < RING_SIZE; index++) {
        init_ring_element(&(r->array[index]), index);
    }

    atomic_store(&(r->head),0);
    atomic_store(&(r->tail),0);
    atomic_store(&(r->next),NULL);
}

static void SHARED_OBJECT_INIT() {
     RingQueue *rq = getMemory(sizeof(RingQueue));
     init_ring(rq);
     atomic_store(&head,rq);
     atomic_store(&tail,rq);
}


//--------------------enqueue & dequeue
inline uint64_t dequeue(const uint64_t arg) {
    RingQueue *rq = (RingQueue*)atomic_load(&head);//updates by atomic_compare_exchange_strong no need to reload in each while iteration 
    RingQueue *next;
    while (1) {

        const uint64_t h = atomic_fetch_add(&rq->head, 1ull);

        uint64_t tt = 0;
        uint64_t r = 0;

        while (1) {
            const RingNode* cell = (const RingNode*)atomic_load(&(rq->array[h & (RING_SIZE-1)]));

            const uint64_t cell_idx = cell->idx;
            const uint64_t unsafe = node_unsafe(cell_idx);
            uint64_t idx = node_index(cell_idx);
            uint64_t val = cell->val;

            if (unlikely(idx > h)) break;

            if (likely(!is_empty(val))) {
                if (likely(idx == h)) {
                    if (CASNode(&(rq->array[h & (RING_SIZE-1)]), cell, -1, unsafe | (h + RING_SIZE)))
                        return val;
                } else {
                    if (CASNode(&(rq->array[h & (RING_SIZE-1)]), cell, val, set_unsafe(idx))) {
                        count_unsafe_node();
                        break;
                    }
                }
            } else {
                if ((r & ((1ull << 10) - 1)) == 0){
                    tt = atomic_load(&rq->tail);
                }

                // Optimization: try to bail quickly if queue is closed.
                int crq_closed = crq_is_closed(tt);
                uint64_t t = tail_index(tt);

                if (unlikely(unsafe)) { // Nothing to do, move along
                    if (CASNode(&(rq->array[h & (RING_SIZE-1)]), cell, val, unsafe | (h + RING_SIZE)))
                        break;
                } else if (t < h + 1 || r > 200 || crq_closed) {
                    if (CASNode(&(rq->array[h & (RING_SIZE-1)]), cell, val, h + RING_SIZE)) {
                        if (r > 200 && tt > RING_SIZE){
                            BIT_63_TEST_AND_SET(&rq->tail);
                        }
                        break;
                    }
                } else {
                    ++r;
                }
            }
        }

        if (tail_index(atomic_load(&rq->tail)) <= h + 1) {
            
            fixState(rq);

            // try to return empty
            next = (RingQueue*)atomic_load(&(rq->next));
            if (next == NULL){
                return -1;  // EMPTY
            }
            if (tail_index(atomic_load(&rq->tail)) <= h + 1){
                atomic_compare_exchange_strong(&head, (uint64_t*)&rq, next);
            }
       }
    }    
}

inline void enqueue(const uint64_t arg) {
    int try_close = 0;
    RingQueue *nrq = NULL;
    RingQueue *rq = (RingQueue*)atomic_load(&tail);//updates by atomic_compare_exchange_strong no need to reload in each while iteration

    while (1) {

        RingQueue *next = (RingQueue*)atomic_load(&(rq->next));
 
        if (unlikely(next != NULL)) {
	       atomic_compare_exchange_strong(&tail,(uint64_t*)&rq,next);
            continue;
        }

        const uint64_t t = atomic_fetch_add(&(rq->tail),1ull);
        if (crq_is_closed(t)) {
    alloc:
            if (nrq == NULL) {
                nrq = getMemory(sizeof(RingQueue));
                init_ring(nrq);
            }
            // Solo enqueue
    	    atomic_store(&(nrq->tail),1ull);
    	    RingNode* newNode=createNode(arg,0ull);
    	    atomic_store(&(nrq->array[0]),newNode);

    	    uint64_t zero=0ull;
    	    if(atomic_compare_exchange_strong(&(rq->next),&zero,nrq)) {
        		atomic_compare_exchange_strong(&tail,(uint64_t*)&rq,nrq);
        		nrq=NULL;
        		return;
	        }
            continue;
        }

           
        const RingNode* cell = (const RingNode*)atomic_load(&(rq->array[t & (RING_SIZE-1)]));

        const uint64_t idx = cell->idx;
        const uint64_t val = cell->val;

        if (likely(is_empty(val))) {
            if (likely(node_index(idx) <= t)) {
                if ((likely(!node_unsafe(idx)) || atomic_load(&(rq->head)) < t) && CASNode(&(rq->array[t & (RING_SIZE-1)]), cell, arg, t)) {
                    return;
                }
            }
        } 

        const uint64_t h = atomic_load(&(rq->head));

        if (unlikely((int64_t)(t - h) >= (int64_t)RING_SIZE) && close_crq(rq, t,++try_close)) {
            count_closed_crq();
            goto alloc;
        }
    }
}




//-----------------testing code below this line--------------------

int crq_contains(atomic_uintptr_t* array, uint64_t value){
    for(uint index = 0; index < RING_SIZE; index += 1){
        if(((const RingNode*)atomic_load(&(array[index])))->val == value)
            return 1;
    }

    return 0;
}
int contains(uint64_t value){
    RingQueue* rq = (RingQueue*)atomic_load(&head); 

    while(rq){
        if(crq_contains(rq->array, value))
            return 1;

        rq = (RingQueue*)atomic_load(&rq->next);
    }

    return 0;
}
int check_array_initialized(atomic_uintptr_t* ring_nodes_array){
   for(uint64_t index = 0; index < RING_SIZE; index += 1){
       const RingNode* element = (const RingNode*)atomic_load(&(ring_nodes_array[index]));
       
       if(element == NULL ||  element->idx != index || element->val != -1){
           return 0;
       }
   }
   
   return 1;
}
int check_queue_image(uint64_t* queue_image, uint64_t image_length,uint64_t rq_index_init_val){
    RingQueue* rq = (RingQueue*)atomic_load(&head);
        while(rq_index_init_val>=RING_SIZE){
            rq_index_init_val-=RING_SIZE;
        }
    for(uint image_index = 0, rq_index = rq_index_init_val; image_index < image_length && rq; image_index += 1, rq_index += 1){
        if(rq_index == RING_SIZE){
            rq_index = 0;
            rq = (RingQueue*)atomic_load(&rq->next);
        }

        if(queue_image[image_index] != ((const RingNode*)atomic_load(&rq->array[rq_index]))->val)
            return 0;    
    }
    return 1;
}
int crq_size(atomic_uintptr_t* array){
    int retVal=0;
    for(uint index = 0; index < RING_SIZE; index += 1){
        if(((const RingNode*)atomic_load(&(array[index])))->val != (uint64_t)-1){
            retVal++;
        }
    }
    return retVal;
}
int queue_size(){
    int retVal=0;
    RingQueue* rq = (RingQueue*)atomic_load(&head); 

    while(rq){
        retVal+=crq_size(rq->array);
        rq = (RingQueue*)atomic_load(&rq->next);
    }
    return retVal;
}

void init_test(){
    SHARED_OBJECT_INIT();
    MODEL_ASSERT(atomic_load(&head));
    atomic_uintptr_t* array=((RingQueue*)atomic_load(&head))->array;
    MODEL_ASSERT((void*)array);
    MODEL_ASSERT(check_array_initialized(array));
}
void enqueue_test(){
    SHARED_OBJECT_INIT();
    enqueue(89);
    enqueue(40);
    enqueue(3);
    MODEL_ASSERT(contains(89));
    MODEL_ASSERT(contains(40));
    MODEL_ASSERT(contains(3));
    uint64_t queue_image[3] = {89, 40, 3};
    MODEL_ASSERT(check_queue_image(queue_image, 3,0));
}
void dequeue_test(){
    SHARED_OBJECT_INIT();
    enqueue(89);
    enqueue(40);
    enqueue(3);
    MODEL_ASSERT(dequeue(1) == 89);
    MODEL_ASSERT(!contains(89));
    MODEL_ASSERT(contains(40));
    MODEL_ASSERT(contains(3));
    uint64_t queue_image[2] = {40, 3};
    MODEL_ASSERT(check_queue_image(queue_image, 2,1));
}
void complex_correctness_test_1(){
    SHARED_OBJECT_INIT();
    enqueue(80);
    enqueue(5);
    enqueue(2);
    MODEL_ASSERT(dequeue(0) == 80);
    MODEL_ASSERT(dequeue(0) == 5);
    MODEL_ASSERT(dequeue(0) == 2);
    enqueue(80);
    enqueue(5);
    uint64_t queue_image[2] = {80, 5};
    MODEL_ASSERT(check_queue_image(queue_image, 2,3));
    MODEL_ASSERT(dequeue(0) == 80);
    enqueue(6);
    uint64_t queue_image2[2] = {5,6};
    MODEL_ASSERT(check_queue_image(queue_image2, 2,4));
}
void complex_correctness_test_2(){
    SHARED_OBJECT_INIT();
    for(uint64_t i=0; i<3*RING_SIZE+2;i++){
        enqueue(i+3);
    }
    for(uint64_t i=0; i<1*RING_SIZE+2;i++){
        MODEL_ASSERT(dequeue(0) == i+3);
    }

    uint64_t queue_image[2*RING_SIZE];
    for(uint64_t i=0;i<2*RING_SIZE;i++){
        queue_image[i]=i+RING_SIZE+2+3;
    }
    MODEL_ASSERT(check_queue_image(queue_image, 2*RING_SIZE,RING_SIZE+2));
}
void correctness_tests(){
    enqueue_test();
    dequeue_test();
    complex_correctness_test_1();
    complex_correctness_test_2();
}


void concurrent_tests_test_enqueuer_dequeuer(void* arg){
    int id=((int*)arg)[0];
    int numOfTimes=((int*)arg)[1];
    int numOfAdded=0;
    for(int i=0;i<numOfTimes;i++){
        enqueue(id*1000+i);
        uint64_t res=dequeue(id*1000+i);
        numOfAdded+=(res==(uint64_t)-1);
    }
    *((int*)arg)=numOfAdded;
}
void concurrent_tests_test_enqueuer(void* arg){
    int id=((int*)arg)[0];
    int numOfTimes=((int*)arg)[1];
    for(int i=0;i<numOfTimes;i++){
        enqueue(id*1000+i);
    }
}
void concurrent_tests_test_dequeuer(void* arg){
    int id=((int*)arg)[0];
    int numOfTimes=((int*)arg)[1];
    int numOfRemoved=0;
    for(int i=0;i<numOfTimes;i++){
        uint64_t res=dequeue(id*1000+i);
        numOfRemoved+=(res!=(uint64_t)-1);
    }
    *((int*)arg)=numOfRemoved;
}
void concurrent_tests(int argc, char **argv){
    int initNodesNumber=0;
    int dequeuers=0;
    int dequeuesPerDequeuer=0;
    int enqueuers=1;
    int enqueuesPerEnqueuer=1;
    int enqueuersDequeuers=0;
    int enqueuesDequeuesPerEnqueuerDequeuer=0;
    int queueExpSize=0;
    if(argc>=7){
        sscanf(argv[0], "%d", &initNodesNumber);
        sscanf(argv[1], "%d", &dequeuers);
        sscanf(argv[2], "%d", &dequeuesPerDequeuer);
        sscanf(argv[3], "%d", &enqueuers);
        sscanf(argv[4], "%d", &enqueuesPerEnqueuer);
        sscanf(argv[5], "%d", &enqueuersDequeuers);
        sscanf(argv[6], "%d", &enqueuesDequeuesPerEnqueuerDequeuer);
    }
    //create threads
    thrd_t* threads=malloc(sizeof(thrd_t)*(enqueuers+dequeuers+enqueuersDequeuers));
    int* thrdArg=malloc(2*sizeof(int)*(enqueuers+dequeuers+enqueuersDequeuers));
    //init queue
    SHARED_OBJECT_INIT();
    int arg[2]={0,0};//{id,num of runs}
    arg[1]=initNodesNumber;
    concurrent_tests_test_enqueuer(arg);
    queueExpSize=initNodesNumber+enqueuesPerEnqueuer*enqueuers;

    //run threads
    for(int i=0;i<enqueuers+dequeuers+enqueuersDequeuers;i++){
        thrdArg[2*i]=i;
        if(i<enqueuers){
            thrdArg[2*i+1]=enqueuesPerEnqueuer;
            thrd_create(threads+i, concurrent_tests_test_enqueuer, thrdArg+2*i);
        }else if(i<enqueuers+dequeuers){
            thrdArg[2*i+1]=dequeuesPerDequeuer;
            thrd_create(threads+i, concurrent_tests_test_dequeuer, thrdArg+2*i);
        }else{
            thrdArg[2*i+1]=enqueuesDequeuesPerEnqueuerDequeuer;
            thrd_create(threads+i, concurrent_tests_test_enqueuer_dequeuer, thrdArg+2*i);
        }
    }
    //join
    for(int i=0;i<enqueuers+dequeuers+enqueuersDequeuers;i++){
        thrd_join(threads[i]);
        if(i>=enqueuers&&i<enqueuers+dequeuers){//dequeue
            queueExpSize-=(thrdArg[2*i]);
        }
        if(i>=enqueuers+dequeuers){//enqueue&dequeue
            queueExpSize+=(thrdArg[2*i]);
        }
    }
    //check queue
    MODEL_ASSERT(queueExpSize == queue_size());
    return;
}

int user_main(int argc, char **argv) {
    int testType=0;
    if(argc>=2){
        sscanf(argv[1], "%d", &testType);
    }
    if(testType==0){
        init_test();
        correctness_tests();
    } else {
        concurrent_tests(argc-2,argv+2);//no need for argv[0,1]
    }
    return 0;
}
