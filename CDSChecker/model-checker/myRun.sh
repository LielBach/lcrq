#!/bin/bash
#runs the test passing 4 args, number 2 to number 5, number 1 is clean (randomly picked the number 4) usage example ./myRun.sh noClean -v3 or ./myRun.sh clean -v3 for a rebuild
if [ "$1" = "clean" ] ; then
	echo cleaning
        make clean
fi

RED='\033[0;31m'
NC='\033[0m' # No Color

echo compiling
make
echo running
echo -e ${RED}run1${NC}
a=$(date +%s%N)
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 0
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run2${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 1 0 0 0 0 0 4 1 
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run3${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 1 100 0 0 0 0 3 2
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run4${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 1 3 0 0 0 0 8 1
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run5${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 1 0 4 1 0 0 0 0
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run6${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 1 5 4 2 0 0 0 0
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run7${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 1 200 2 6 0 0 0 0
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run8${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 1 0 0 0 5 3 0 0
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run9${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 1 30 0 0 2 2 0 0
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run10${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 1 31 2 1 2 2 0 0
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run11${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 1 10 2 2 2 2 0 0
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run12${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 1 0 2 1 2 2 0 0
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run13${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 10 -- 1 31 2 1 2 1 1 1
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run14${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 9 -- 1 35 35 1 0 0 1 1
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

a=$(date +%s%N)
echo -e ${RED}run15${NC}
./run.sh test/lcrq.o $2 $3 $4 $5 -m 2 -f 9 -- 1 31 35 1 2 1 1 1
b=$(date +%s%N)
diff=$(($b - $a))
echo took $diff nano sec

echo done
